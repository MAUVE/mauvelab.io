## Configurable Element

In MAUVE Runtime, almost all elements are **configurable**: the shells, the cores,
the finite state machines, the components, the resources and the architectures.

Each configurable element can be in (at least) two states: *not configured* or *configured*.
* At first, the element is not configured.
* Calling the method `configure()` can change its state into *configured* if success.
* Calling the method `cleanup()` changes its state into *not configured*.

The following figure shows the "configurable state machine":
<p align="center">
  <img src="/images/configurable.png">
</p>

### Hooks

The behavior of the `configure()` and `cleanup()` can be changed by overriding the corresponding hook: `configure_hook()` and `cleanup_hook()`.

In the following example, the `configure()` method of shell `MyShell` will
succeed if and only if the value of the property `age` is greater or equals to *25*.
Cleaning up the component will reset its age to *0*.

```c++
struct MyShell: public Shell {
  Property<int> & age = mk_property<int>("age", 0);

  bool configure_hook() override {
    if (this.age < 25) {
      return false;
    }
    return true;
  }

  void cleanup_hook() override {
    this.age = 0;
  }
};
```
