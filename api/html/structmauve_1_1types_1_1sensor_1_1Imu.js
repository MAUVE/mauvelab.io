var structmauve_1_1types_1_1sensor_1_1Imu =
[
    [ "angular_velocity", "structmauve_1_1types_1_1sensor_1_1Imu.html#ac37a175491e83ea8f39c09f4cc05edb0", null ],
    [ "linear_acceleration", "structmauve_1_1types_1_1sensor_1_1Imu.html#a77b2afd83db30e2f17bf549ca728411e", null ],
    [ "magnetometer", "structmauve_1_1types_1_1sensor_1_1Imu.html#adcf85f4161004d3d39751e5c2cb239e1", null ],
    [ "orientation", "structmauve_1_1types_1_1sensor_1_1Imu.html#a6b9fb3085d69cdda3baf490b39b67f7f", null ],
    [ "timestamp", "structmauve_1_1types_1_1sensor_1_1Imu.html#a417f59385f15fc36fe7c7df740fb1d60", null ]
];