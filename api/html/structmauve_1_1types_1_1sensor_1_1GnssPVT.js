var structmauve_1_1types_1_1sensor_1_1GnssPVT =
[
    [ "altitude", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a898077bdfa7ba33212a6d90d66c96428", null ],
    [ "amsl", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a2257f4eb0654846b592add746ef00d21", null ],
    [ "ground_speed", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#ae4c587b25e03d47a26cbaba7d7de5edd", null ],
    [ "heading_acc", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a90d15663509b5474ff05dba3f060cbd2", null ],
    [ "horizontal_acc", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#ad47bef53b36ce531b56758a6eb4f4e27", null ],
    [ "latitude", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#ae4f27ec3b39620d7a8d0028ed77f33fe", null ],
    [ "longitude", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#ad5368257d59faa0ec94aba930388f20e", null ],
    [ "mag_acc", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a12d825bdeb1689a6f04ac99278d19e55", null ],
    [ "mag_dec", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#af545f8debef0146405f5e252493160e4", null ],
    [ "motion_heading", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a1eb81688abf7309a743431d729f997f2", null ],
    [ "p_dop", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a0b09b110a7f19e76aacc9e5b62c783f5", null ],
    [ "speed_acc", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#ac520c2fef92acfde3b03eb6c953634d9", null ],
    [ "status", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#af08c5653289f078f03327e04e1c161e6", null ],
    [ "time", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a93015c6e93fcac786f57dbb4fc14e403", null ],
    [ "timestamp", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a4b3eb4396b56cc8c2e04e91f2621e7ff", null ],
    [ "vehicle_heading", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a2678fe67dcbfd50c895b3107eeb4af00", null ],
    [ "velocity_D", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a8c799117e8ffeb965c274576a167b547", null ],
    [ "velocity_E", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#ac04f00ab31603ab69aab4eff00a93230", null ],
    [ "velocity_N", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a432aa41522a0cbdd02a4b0ec93dd4783", null ],
    [ "vertical_acc", "structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a5339ad706b8c67763058a65f4de14680", null ]
];