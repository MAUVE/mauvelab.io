var searchData=
[
  ['sec',['sec',['../structmauve_1_1types_1_1sensor_1_1GnssTime.html#af3336f765ba42227fd20e1617443f9c4',1,'mauve::types::sensor::GnssTime']]],
  ['select',['select',['../structmauve_1_1base_1_1MuxReadPortShell.html#a0b3245e9c76a24f0f877cf3092a7ad72',1,'mauve::base::MuxReadPortShell::select()'],['../structmauve_1_1base_1_1MuxWriteServiceShell.html#a3df59bd9febf3be46558b5c45e7699e3',1,'mauve::base::MuxWriteServiceShell::select()']]],
  ['service',['service',['../structmauve_1_1types_1_1sensor_1_1GNSSStatus.html#aae4148675f6f9d7d214b98b78bffb69d',1,'mauve::types::sensor::GNSSStatus']]],
  ['speed_5facc',['speed_acc',['../structmauve_1_1types_1_1sensor_1_1GnssPVT.html#ac520c2fef92acfde3b03eb6c953634d9',1,'mauve::types::sensor::GnssPVT::speed_acc()'],['../structmauve_1_1types_1_1sensor_1_1GnssVelocity.html#ae834463ab9c70c7d43bf6bb5e669654b',1,'mauve::types::sensor::GnssVelocity::speed_acc()']]],
  ['status',['status',['../structmauve_1_1types_1_1sensor_1_1GnssPVT.html#af08c5653289f078f03327e04e1c161e6',1,'mauve::types::sensor::GnssPVT::status()'],['../structmauve_1_1types_1_1sensor_1_1GNSSStatus.html#af866bf4bd6df4d81ce05763f69ddfa18',1,'mauve::types::sensor::GNSSStatus::status()']]],
  ['sv_5fid',['sv_id',['../structmauve_1_1types_1_1sensor_1_1SatInfo.html#a05ac7fa1e54bbc11df19b52f4f8820d3',1,'mauve::types::sensor::SatInfo']]]
];
