## Traca (TRACe Analysis)

*Traca* toolchain is mode to analyse executions traces. 
Some specific extensions has been made to ease the analysis of Mauve traces.

The principle is the following one:
1. activate [MAUVE Tracing](../manual/tracing/mauve_tracing.md)
2. run your system
3. save the lttng trace
4. *analyse your trace with Traca*


## Install instructions

### Preriquisite

Traca tool is written is [Scala](www.scala-long.org), so the java virtual machine is needed.
More precisely the *Java8* (1.8.xxx) VM is required.

To check your currently installed version:
```sh
java -version
```

### Download

Traca is distributed as a self-contained jar file; just download it: [mauve_traca.jar](https://gitlab.com/MAUVE/mauve_traca/tree/master/release/mauve_traca.jar).


### Launch

Traca embedded a customized version of the Scala REPL.
Consequently, once Traca laucnhed you have access to the complete Scala language and also some specific commands for the trace analysis.

To run Traca:
```sh
java -cp release/mauve_traca.jar mauve.traca.pointwise.MainConsole
```