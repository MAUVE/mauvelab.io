# MAUVE Runtime Logger

MAUVE Runtime provides a logging infrastructure settled on [spdlog](https://github.com/gabime/spdlog).
In your source code, it allows to log message of several levels to categories.
At configuration time, it allows to decide which category logs at which level, and how.

## MAUVE Loggers

By default, MAUVE Runtime provides loggers (corresponding to categories) for:
- the deployer (as `runtime` category)
- each component of the architecture (component name is used as category)
- each resource of the architecture (resource name is used as category)
- custom categories

### Log levels

Available log levels are: _trace_, _debug_, _info_, _warn_, _error_ and _critical_.

### Log formats

Formatting a message for logging is very simple and uses parameter substitution. All logging
methods take as first argument a formatted string, and as following arguments the
parameters to be substituted.
See "http://fmtlib.net/latest/syntax.html" for the syntax description.

## Logging from source code

### From components and resources

Component and resource objects already have a logger configured. Hence, when you define
your own shell/core/fsm/interface, you can use the logger to log message to the corresponding
category. For instance, the following code defines a new core type `C`, and in its
`update` method, the data read from `read_port` is logged to the component logger
with level _info_. Each log level has its corresponding method.

```c++
struct C: public Core<S> {
  void update() {
    int i = shell().read_port.read();
    this->logger().info("read {}", i);
  }
};
```

### The 'runtime' category

The runtime category is generally used to log specific MAUVE Runtime messages
corresponding to configuration and starting of components, threads, ...
It is not advised to use this logger from user code.

### Logging to a custom category

If you want to create your own category, for instance to split log messages
coming from you component into several categories, you can create and access
a new one using the `CategoryLogger` class:

```c++
struct C: public Core<S> {
  void update() {
    int i = shell().read_port.read();
    CategoryLogger("my_category").info("read {}", i);
  }
};
```

## Configuring the logging infrastructure

### YAML configuration

Logging configuration follows the YAML format.
If you used the `mauve::runtime::main` function in your [deployment](deployment.md#deployment-processes), then you can use the *--logger* option to configure the logger from a YAML file. An example of a YAML file is given below to illustrate the corresponding syntax:

```yaml
default:
  level: debug
  type: stdout
cpt_1:
  - type: stdout
    level: info
cpt_2:
  - type: stdout
    level: info
  - type: rosjson
    level: info
```

This code configures:
* the default logger (_runtime_ plus all not configured categories):
  * its logging level is _debug_
  * the log messages go to _stdout_
* the logger for component *cpt_1*:
  * its logging level is _info_
  * the log messages go to _stdout_
* the logger for component *cpt_2*:
  * the log messages go to _stdout_ with level _info_ and to _rosjson_ with level _info_

### Logging _sinks_

MAUVE Runtime logging supports the following types (i.e. places where the log are sent/written):

* _stdout_: logs are printed to the standard output
* _stderr_: logs are printed to the error output
* _file_: logs are printed to a file; the _filename_ can be specified as a supplementary argument
* _daily\_file_: logs are printed to a new file each day; the _filename_, as well as the _hour_ and _minutes_
  at which the new file must be created can be specified as a supplementary argument
* _rotating\_file_: logs are printed to new files when the max. file size is reached; the _filename_, as
  well as the _max\_size_ and _max\_files_ can be specified as a supplementary argument
* _rosjson_: logs are sent in JSON format on a UDP port; this logging type is compliant with *rosbridge* and allows to display MAUVE logs in the ROS console; _ip_ and _port_ of the rosbridge server can be specified as a supplementary argument

### Logging to ROS console

The _rosjson_ logging type allows to send log messages to the *rosbridge* server using UDP. This server
parses the JSON streams sent by MAUVE and produces ROS log messages to the */rosout* topic. MAUVE
log messages can then be displayed, for instance, in *rqt_console*.
To use this logging, you can simply launch the rosbridge UDP server:

```
roslaunch rosbridge_server rosbridge_udp.launch
```

Then launch the console UI: `rqt_console`

## MAUVE Runtime logs

MAUVE Runtime already uses logging to log important messages. Log messages provided by the MAUVE Runtime (with logging levels indicated within parentheses) are:

* for each FSM:
  * (trace) the state name being run
  * (trace) the transition being tested
  * (trace) the state to which the FSM goes
* for each WriteService/EventService/CallService/ReadService:
  * (trace) the service being called
* in the _runtime_ category:
  * (debug) logger creation and configuration
  * (warn) if the configured logger is unknown
  * (warn) deployment issues when trying to setup component clocks, or to get system clock
  * (error) deployment errors when trying to setup component clocks, or to get system clock
  * (info) start/stop of the deployment
  * (debug) creation/activation/start/stop/clearing of tasks
  * (info) architecture configuration/cleaning
  * (debug) creation/configuration/cleaning of Shells, Cores, FSMs, Interfaces
* for each component:
  * (warn) RT configuration issues
  * (error) task creation errors
