# Shell

The shell describes the interface of a component or a resource.
It contains ports and properties.
The shell is the only visible part outside the component/resource.

A Shell is created by extending the `mauve::runtime::Shell` class.

```c++
#include "mauve/runtime.hpp"

struct MyShell: public mauve::runtime::Shell {};
```

A Shell is [configurable](configurable.md), you can then override the _configure_
and _cleanup_ hooks.

## Shell Property {#shell-property}

Properties can be defined in the Shell as class attributes. The Property class is templated by the underlying type of the property. The `mk_property` method allows to construct the property and returns a reference on the property object. This method has two arguments: the name of the property (usefull to change the property from configuration files) and a default value.

For instance, the code

```c++
struct MyShell: public mauve::runtime::Shell {
  mauve::runtime::Property<int> & age = mk_property<int>("age", 0);
};
```

defines a property attribute _age_ of type _int_, with name _age_ and a default value of 0.

Shell properties will be accessible from the shell, the core, and  
the finite state-machine of the component, and from the architecture.

## Shell Port {#shell-port}

Ports are used by components in order to communicate to resources.  
There is 4 types of ports: `ReadPort`, `WritePort`, `CallPort`, and `EventPort`. To be functionnal, a _Port_ needs to be bound to a _Service_ of the same type \(see _Interface_\).

For instance, the code

```c++
struct MyShell: public mauve::runtime::Shell {
  mauve::runtime::ReadPort<int> & read_age = mk_read_port<int>("read_age", 0);
  mauve::runtime::WritePort<int> & write_age = mk_write_port<int>("write_age");
  mauve::runtime::CallPort<int, std::string> & call_age = mk_call_port<int, std::string>("call_age");
  mauve::runtime::EventPort<> & event_age = mk_event_port<>("event_port");
};
```

defines:

* a read port _read\_age_ of type _int_, with name _read\_age_ and a default value of 0
* a write port _write\_port_ of type int with name _write\_age_
* a call port _call\_port_ of return type _int_, parameter type _std::string_, with name _call\_age_
* an event port _event\_port_ with name _event\_port_
