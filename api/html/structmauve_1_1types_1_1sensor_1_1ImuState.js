var structmauve_1_1types_1_1sensor_1_1ImuState =
[
    [ "angular_velocity", "structmauve_1_1types_1_1sensor_1_1ImuState.html#aeb515cbcebc131d105adbe8dc5e12053", null ],
    [ "angular_velocity_covariance", "structmauve_1_1types_1_1sensor_1_1ImuState.html#acc6279139bbf5eb7f2e0fcf390f2ac72", null ],
    [ "angular_velocity_valid", "structmauve_1_1types_1_1sensor_1_1ImuState.html#a28f12fc52dc70082377780dcf2fe6214", null ],
    [ "ekf_heading_valid", "structmauve_1_1types_1_1sensor_1_1ImuState.html#a418ffa2e1da1d31a0df20903373039a4", null ],
    [ "ekf_init_valid", "structmauve_1_1types_1_1sensor_1_1ImuState.html#ab543e9ba77ac065f9a14db7c069b33dd", null ],
    [ "ekf_stamp", "structmauve_1_1types_1_1sensor_1_1ImuState.html#a34c7006664c58a6be58a48172b7f7279", null ],
    [ "imu_stamp", "structmauve_1_1types_1_1sensor_1_1ImuState.html#aafc96bcb1e79957fe390e38891ab1d40", null ],
    [ "linear_acceleration", "structmauve_1_1types_1_1sensor_1_1ImuState.html#a70de62afe9fff8daaa9b7de740b956e5", null ],
    [ "linear_acceleration_covariance", "structmauve_1_1types_1_1sensor_1_1ImuState.html#ad4ecfc3d59e38b442a874ae15af325e2", null ],
    [ "linear_acceleration_valid", "structmauve_1_1types_1_1sensor_1_1ImuState.html#a34c4758fc41e67918389605713c3e807", null ],
    [ "mag_stamp", "structmauve_1_1types_1_1sensor_1_1ImuState.html#adb477db5cda79f1ff4d616bf2029546d", null ],
    [ "magnetic_field", "structmauve_1_1types_1_1sensor_1_1ImuState.html#a280cb57cbe0dd05f2e68c4f398f68dd3", null ],
    [ "magnetic_field_covariance", "structmauve_1_1types_1_1sensor_1_1ImuState.html#a7132397941eec0b9bbcfb7eb6456cbc6", null ],
    [ "magnetic_field_valid", "structmauve_1_1types_1_1sensor_1_1ImuState.html#a4cf09e229b0097c0067ff8247bcc2094", null ],
    [ "orientation", "structmauve_1_1types_1_1sensor_1_1ImuState.html#a07532edfa001892b6dcafc91cc417671", null ],
    [ "orientation_covariance", "structmauve_1_1types_1_1sensor_1_1ImuState.html#a41ce3548d6f126f963fd5584e438d7e7", null ],
    [ "timestamp", "structmauve_1_1types_1_1sensor_1_1ImuState.html#a1e6f5205f32373934511e657d7e46c3c", null ]
];