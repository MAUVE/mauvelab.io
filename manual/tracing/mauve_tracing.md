# MAUVE Tracing

The MAUVE Tracing library defines trace structures
embedded in MAUVE architectures. The tracing system uses
[LTTng](http://lttng.org/docs/v2.9).

MAUVE Tracing provides tracepoints to collect traces on MAUVE runtime
internal elements, and tracepoints to add user-defined trace messages.

## LTTng tracepoints

LTTng structures each tracepoint with a namespace, an event name, and fields.
All the MAUVE traces are in the _mauve_ namespace.

The tracepoints embedded in MAUVE Runtime are:
* _component_start_ when a component is started; its fields are the component name and its clock value;
* _state_execution_begin_ when the execution of a component's state starts; its fields are the component name, its clock value and the state name;
* _state_execution_end_ when the execution of a component's state ends; its fields are the component name, its clock value and the state name.
* _service_execution_begin_ when the execution of a resource's service starts; its fields are the resource name and the service name;
* _service_execution_end_ when the execution of a resource's service ends; its fields are the resource name and the service name.

## Activating trace collection

The structure of the LTTng components is available at http://lttng.org/docs/v2.9/#doc-plumbing.

### Starting trace collection

To activate LTTng tracing and start collection, launch the following command:
```
rosrun mauve_tracing lttng-start [session]
```

Default session name is _mauve_. The trace is registered in `~/lttng-traces/<session>-<date>/`.

Example: `~/lttng-traces/mauve-20170711-132012/`.

If you want to filter the traces to be collected, look at the [advanced LTTng usage](advanced.md).

### Stoping trace collection

To stop trace collection and close the session, launch the following command:
```
rosrun mauve_tracing lttng-stop
```

### Reading the traces

Once the trace session is destroyed, you can read the collected traces. LTTng links to several tools:

http://lttng.org/docs/v2.10/#doc-viewing-and-analyzing-your-traces

For instance, using _babeltrace_, you can have a readable form of the traces:
```sh
$ babeltrace ~/lttng-traces/mauve-20170711-132012/
(...)
[13:21:27.815739685] (+0.000010232) wdcsd002h mauve:state_execution_end: { cpu_id = 1 }, { name = "/B", clock = 1499772087803282475, state = "burn" }
[13:21:27.815740296] (+0.000000611) wdcsd002h mauve:state_execution_begin: { cpu_id = 1 }, { name = "/B", clock = 1499772087803282475, state = "sleep" }
[13:21:27.815751168] (+0.000010872) wdcsd002h mauve:state_execution_end: { cpu_id = 1 }, { name = "/A", clock = 1499772087803282475, state = "sleep" }
[13:21:27.815751650] (+0.000000482) wdcsd002h mauve:state_execution_begin: { cpu_id = 1 }, { name = "/A", clock = 1499772087803282475, state = "burn" }
```
