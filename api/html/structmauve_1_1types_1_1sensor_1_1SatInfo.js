var structmauve_1_1types_1_1sensor_1_1SatInfo =
[
    [ "azim", "structmauve_1_1types_1_1sensor_1_1SatInfo.html#a950c65e86ed4031b5d21cc6ae9122496", null ],
    [ "cno", "structmauve_1_1types_1_1sensor_1_1SatInfo.html#ad32725102d350753a9aa202defe878c0", null ],
    [ "elev", "structmauve_1_1types_1_1sensor_1_1SatInfo.html#ad9e1ba6d854948b095baabd663daf19c", null ],
    [ "flags", "structmauve_1_1types_1_1sensor_1_1SatInfo.html#a74834563ebfcbc74a85be3267282ecdd", null ],
    [ "gnss_id", "structmauve_1_1types_1_1sensor_1_1SatInfo.html#a83d8a91e88a7baaa6315cf47a084b0c9", null ],
    [ "pr_res", "structmauve_1_1types_1_1sensor_1_1SatInfo.html#a855d01593b2286da69c1398ace192a4a", null ],
    [ "sv_id", "structmauve_1_1types_1_1sensor_1_1SatInfo.html#a05ac7fa1e54bbc11df19b52f4f8820d3", null ]
];