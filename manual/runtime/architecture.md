# Architectures

An Architecture combines components and resources instances, and
connect and configure them.

To define an architecture, you must ineherit the *Architecture* class:

```c++
struct MyArchitecture : public ::mauve::runtime::Architecture {
}
```

## Components

Components are declared as architecture attributes. A *Component* type is defined
by its Shell, Core, and FSM. The following line declares a component *cpt*:

```c++
  Component<MyShell, MyCore, MyFSM> & cpt = mk_component<MyShell, MyCore, MyFSM>("mycomponent");
```

The component *cpt* has name *mycomponent*. Its base shell, core and FSM types are respectively *MyShell*, *MyCore* and *MyFSM*.

## Resources

Resources declaration follow the same scheme as components. MAUVE Runtime already provides two kinds of resources: *SharedData*, that own a unique data shared between several components, and a *RingBuffer*, implementing a circular buffer of data.
They can be declared using the following code:

```c++
  SharedData<int> & data = mk_resource< SharedData<int> >("data", 0);
  RigBuffer<double> & buffer = mk_resource< RingBuffer<double> >("buffer", 5, 0);
```

*data* is a shared data named "data" owning an integer value, initially of value 0.
*buffer* is a ring buffer of size 5, named "buffer", containing double values, and initially filled with 0.

## Configuration

Attributes of an architecture allow to declare components and resources that are contained in the architecture.

Configuration of the architecture is done within its `configure_hook` method:
```c++
struct MyArchitecture: public mauve::runtime::Architecture {
  Component<MyShell, MyCore, MyFSM> & cpt_1 = mk_component<MyShell, MyCore, MyFSM>("cpt_1");
  Component<MyShell, MyCore, MyFSM> & cpt_2 = mk_component<MyShell, MyCore, MyFSM>("cpt_2");
  SharedData<int> & data = mk_resource<SharedData<int>>("data", 0);

  bool configure_hook() override {
    cpt_1.shell().age = 5;

    cpt_1.shell().read_port.connect(data.interface().read_value);
    cpt_2.shell().write_port.connect(data.interface().write);

    cpt_1.set_priority(1);
    cpt_2.set_priority(2);
    cpt_1.set_cpu(1);
    cpt_2.set_cpu(2);

    return mauve::runtime::Architecture::configure_hook();
  }
};
```

This architecture contains two components *cpt_1* and *cpt_2*, and a shared data *data*.
The architecture configures the Shell property *age* of *cpt_1* to 5. The Shell property *age* of *cpt_2* takes its default value.
Then, the *write_port* of *cpt_2* is connected to the *write* service of *data*: data written by *cpt_2* are stored into the shared data. This data is read by *cpt_1* in its *read_port*, connected to the *data* interface *read_value*.

The architecture can also define the real-time priorities of the components, and the CPU on which they are executed.
