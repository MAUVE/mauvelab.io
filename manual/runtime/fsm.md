## Finite State Machine

The Finite State Machine \(FSM\) describes how the Component behaves.  
Note that only Components use FSMs, Resources use Interfaces instead (see [Interfaces](interface.md)).

A FSM is created by extending the class `mauve::runtime::FiniteStateMachine`, templated by the `Shell` and `Core` used by this FSM.

```c++
#include "mauve/runtime.hpp"

struct MyFSM: public mauve::runtime::FIniteStateMachine<MyShell, MyCore> {
  /* ... */
};
```

FSMs are built using two state types: Execution States and Synchronization States.  
You must define the states, the initial state, and the transitions of the FSM
in the `configure_hook` \(see Constructing a simple FSM\).  
You can define either an Execution State or a Synchronization State as initial state.
There is no restriction about state transitions.

## Execution State {#fsm-execution}

Each Execution State must be bound to a single function/method on declaration.

```c++
mauve::runtime::ExecState<MyCore> & execState = mk_execution("execState", &MyCore::execMethod);
```

Here, we define an Execution State _execState_, named _execState_, and calling `MyCore::execMethod`.

## Synchronization State {#fsm-synchronization}

Each Synchronization State must have its synchronization time set on declaration.

```c++
mauve::runtime::SynchroState<MyCore> & syncState = mk_synchronization("syncState", mauve::runtime::ms_to_ns(100));
```

Here, we define a Synchronization State _syncState_, named _syncState_, and with a synchronization time of 100ms.

Note that the synchronization time must be given in nanoseconds, but we provide some helper functions to convert from
other units, like `ms_to_ns` and `sec_to_ns`.

## Constructing a simple FSM {#fsm-constructing-simple}

As said above, you must define an initial state and the transitions in order to setup the FSM.

```c++
bool MyFSM::configure_hook() {
  set_inital(execState);
  set_next(execState, syncState);
  set_next(syncState, execState);
  return FiniteStateMachine::configure_hook();
}
```

Here, we define _execState_ as the initial state, _syncState_ following _execState_, and then _execState_ following _syncState_.  
The resulting FSM can be drawn as:

<p align="center">
  <img src="/images/SimpleFSM.png">
</p>

## Constructing a complex FSM {#fsm-constructing-complex}

FSMs can also handle conditional transitions using _mk\_transition_. Each of these transitions must be bound to a  function/method returning a _bool_.

```c++
bool MyFSM::configure_hook() {
  set_inital(execState);
  mk_transition(execState, &MyCore::sleeping, execState);
  set_next(execState, syncState);
  set_next(syncState, execState);
  return FiniteStateMachine::configure_hook();
}
```

Here, we define a conditional reflexive transition on _execState_. While `MyCore::sleeping()` returns true, the FSM will loop on _execState_.  
The resulting FSM can be drawn as:

<p align="center">
  <img src="/images/ComplexFSM.png">
</p>

When several conditional transitions from the same state are defined, they are evaluated by order of definition. If all the conditional transition fails, the default transition (defined using `set_next`) fires. You should define only one outgoing transition per state using `set_next`.
