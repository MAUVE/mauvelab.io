## Mauve Finite State Machine

```scala
Mauve [10]: val FSM = trace.componentsFSM
FSM: Map[String,mauve.traca.pointwise.FSM] = Map(ublox_driver -> FSM(ublox_driver))
```

```scala
Mauve [12]: print(FSM("ublox_driver").pretty)
23,09 % :    ReadSync[E] [7639]
	 99,90 % ->   ReadFrame[E] [7631] 
	  0,10 % ->    WaitSync[S] [8] 
23,07 % :   ReadFrame[E] [7631]
	100,00 % ->  ParseFrame[E] [7631] 
23,07 % :  ParseFrame[E] [7631]
	 66,64 % -> UpdatePorts[E] [5085] 
	 33,36 % ->    ReadSync[E] [2546] 
15,37 % :  WaitPeriod[S] [5085]
	100,00 % ->    ReadSync[E] [5085] 
15,37 % : UpdatePorts[E] [5085]
	100,00 % ->  WaitPeriod[S] [5085] 
 0,02 % :    WaitSync[S] [8]
	100,00 % ->    ReadSync[E] [7] 
```


```scala
Mauve [14]: print(FSM("ublox_driver").prettyStatesDuration)
total duration =   4 sec 761 ms 848 us  47 ns
83,95 % :   ReadFrame[E] [  3 sec 997 ms 333 us 478 ns]
11,07 % :  ParseFrame[E] [  0 sec 527 ms 308 us 753 ns]
 3,69 % :    ReadSync[E] [  0 sec 175 ms 571 us 520 ns]
 1,29 % : UpdatePorts[E] [  0 sec  61 ms 634 us 296 ns]
```

```scala
Mauve [15]: print(FSM("ublox_driver").prettyStatesTiming)
ReadSync[E] [22917]
- active:
    min:   0 sec   0 ms  11 us  80 ns
    avg:   0 sec   0 ms   7 us 661 ns
    max:   0 sec   0 ms  62 us 108 ns
- duration:
    min:   0 sec   0 ms  11 us  80 ns
    avg:   0 sec   0 ms   7 us 661 ns
    max:   0 sec   0 ms  62 us 108 ns
- response:
    min:   0 sec -73 ms -710 us -947 ns
    avg:   0 sec   0 ms  47 us 731 ns
    max:   0 sec   0 ms 422 us 298 ns
ParseFrame[E] [22893]
- active:
    min:   0 sec   0 ms  20 us 131 ns
    avg:   0 sec   0 ms  23 us  33 ns
    max:   0 sec   0 ms 194 us 755 ns
...
```

```scala
Mauve [16]: print(trace.prettyComponentsDuration)
total duration =   4 sec 761 ms 848 us  47 ns
100,00 % : ublox_driver [  4 sec 761 ms 848 us  47 ns]
```
