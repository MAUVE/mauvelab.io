# Activating LTTng traces

## Advanced usage

When you want to collect traces, you must :
* create a session
* enable the event you want to collect
* start trace collection

### Creating a session

To create a session, use the command
```
lttng create
```

It will create a session in the folder `${HOME}/lttng-traces/` within a sub-folder
named _auto_ followed by the current date.

You can specify the session name with
```
lttng create mysession
```
The folder name will then be _mysession_ followed by the current date.

### Enabling events

You can enable a lot of events when using LTTng, from the user and the kernel space, from several namespaces, and even filter the events you want to collect.

To collect MAUVE traces, use
```
lttng enable-event -u mauve:*
```
It enable user event (`-u`) with name `mauve:*`, meaning all the events in namespace _mauve_.

### Starting and stopping trace collection

To start trace collection, use
```
lttng start
```

To stop trace collection, use
```
lttng stop
```

You can use these commands at any time while your program is running.

### Closing the session

In order to use your session traces, you must close the session
```
lttng destroy
```
