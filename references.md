# Material

### MAUVE Runtime

{{ "doose2017joser" | cite }}: [paper](files/joser17_mauve.pdf)

{{ "DooseGL17" | cite }}: [paper](files/DooseGL17.pdf), [slides](files/DooseGL17Slides.pdf)

### Real-Time Analysis

{{ "GobillotGDGLS16" | cite }}: [paper](files/iros16_mauve.pdf), [slides](files/iros16_mauve_slides.pdf)

{{ "GobillotDLS15" | cite }}: [paper](files/etfa15.pdf), [slides](files/etfa15slides.pdf)

### MAUVE DSL

{{ "GobillotLD14" | cite }}: [paper](files/simpar14paper.pdf), [slides](files/simpar14slides.pdf)

# References

{% references %}
{% endreferences %}

# Bibtex

[Bibtex file](literature.bib)
