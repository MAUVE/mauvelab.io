## Using MAUVE with RealTime properties

The instructions below aim at configuring your Linux system to have
the necessary rights to launch your components using the real-time scheduler.

* Create a realtime group:
```sh
sudo groupadd realtime
```

* Add user `user` to the reatime group:
```sh
sudo usermod -a -G realtime user
```

* Add the following lines in /etc/security/limits.conf
```sh
@realtime       soft    cpu             unlimited
@realtime       -       rtprio          100
@realtime       -       memlock         unlimited
```
