## Install instructions

The MAUVE toolchain is made of several packages intended to
provide several functionnalities to design software architectures.

To install the complete toolchain, with all base packages, follow the
instructions below.

These inscructions are for Ubuntu LTS 16.04.

### Compiler

Due to several bugs in the gcc compilers we highly reconmend to use *clang++* compiler (version 3.8 or higher).

```sh
sudo apt install clang clang-3.8
```

On ubuntu you can change your default C and C++ compiler by using update alternative command.

```sh
update-alternatives --config cc
```

```sh
update-alternatives --config c++
```

### Dependencies

The install the complete toolchain, you need the following dependencies:

* Python (runtime interpreter and general language for scripts)
* Jinja2 (a python library for template programming)
* YAML C++ binding (for configuration files)
* LTTng (for real-time trace generation)
* Doxygen (for compiling the API documentation)
* ROS

To install ROS, follow the instructions at http://wiki.ros.org/indigo/Installation.
We advise to install the `ros-kinetic-desktop` suite.

Other dependencies can be installed on the last Ubuntu LTS using:

```sh
sudo apt install \
  python ipython python-jinja2 \
  libyaml-cpp-dev doxygen \
  liblttng-ust-dev lttng-tools
```

### Installing in a Catkin workspace

First you need to source the ROS setup:

```sh
source /opt/ros/kinetic/setup.bash
```

Then create a workspace and install the toolchain:
```sh
mkdir -p ~/mauve_ws/src
cd ~/mauve_ws
git clone https://gitlab.com/mauve/toolchain/mauve_tracing.git src/mauve_tracing
git clone https://gitlab.com/mauve/toolchain/mauve_runtime.git src/mauve_runtime
git clone https://gitlab.com/mauve/toolchain/mauve_types.git src/mauve_types
git clone https://gitlab.com/mauve/toolchain/mauve_ros.git src/mauve_ros
catkin_make
```

Then to use the MAUVE toolchain for your own architectures and developments:
```sh
source ~/mauve_ws/devel/setup.bash
```
