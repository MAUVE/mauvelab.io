# MAUVE ROS

The *mauve_ros* library provides a binding between MAUVE and ROS, thanks to
several elements:

* a `PublisherResource`, that allows to publish data to a ROS topic by writing
into a resource, just as writing into a classical shared resource ;

* a `SubscriberResource`, that allows to subscribe to data coming from a ROS topic by reading a resource, just as reading a classical shared resource ;

* a `PublisherComponent` and a `SubscriberComponent`, for cases where the data to publish/subcribe is only managed by a MAUVE resource, and then an activity is needed to manage communication with ROS.

All this elements have parameters to define the ROS topic name, the MAUVE and ROS data types, and the conversion between them.
