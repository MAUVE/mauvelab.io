---

# MAUVE Toolchain

---

## The MAUVE Toolchain

MAUVE is a design and validation toolchain for developing software
architectures of robots, or more generally autonomous systems and
cyber-physical systems.
MAUVE settles on the paradigm of component-based architectures, and integrates:
* a Runtime to develop components and component-based architectures and deploy them in real-time,
* Tools to analyze real-time traces, evaluate properties, check schedulability, implement reconfigurations, ...

The toolchain contains the following packages:
* MAUVE [Tracing](/manual/tracing/mauve_tracing.md) ([git repo](https://gitlab.com/mauve/mauve_tracing)),
  an LTTng tracepoint library to log real-time traces of components;
* MAUVE [Runtime](/manual/runtime/runtime.md) ([git repo](https://gitlab.com/mauve/mauve_runtime)),
  a C++ API to develop and deploy architectures made of components, that have real-time activities,
  and resources that own data or processing shared between components;
* MAUVE [Types](/manual/types.md) ([git repo](https://gitlab.com/mauve/mauve_types)),
  a library of standard robotic types, partly inspired from ROS messages;
* MAUVE [ROS](/manual/ros/mauve_ros.md) ([git repo](https://gitlab.com/mauve/mauve_ros)),
  specific resources to publish/subscribe data from ROS topics,
  along with conversion function between ROS messages and specific structures of your architecture;
* MAUVE [Tutorials](/tutorials/README.md) ([git repo](https://gitlab.com/mauve/mauve_tutorials)),
  that contains example files based on the tutorials available on the MAUVE web page.

<p align="center">
  <img src="images/mauve.png">
</p>
