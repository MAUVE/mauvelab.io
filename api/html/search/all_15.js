var searchData=
[
  ['valid',['valid',['../structmauve_1_1types_1_1sensor_1_1GnssTime.html#a04484f96bd2d3258583fbf97404633af',1,'mauve::types::sensor::GnssTime']]],
  ['vector3',['Vector3',['../structmauve_1_1types_1_1geometry_1_1Vector3.html',1,'mauve::types::geometry']]],
  ['vehicle_5fheading',['vehicle_heading',['../structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a2678fe67dcbfd50c895b3107eeb4af00',1,'mauve::types::sensor::GnssPVT::vehicle_heading()'],['../structmauve_1_1types_1_1sensor_1_1GnssVelocity.html#acddb91a5defe0cf61c6bc5cb4455505e',1,'mauve::types::sensor::GnssVelocity::vehicle_heading()']]],
  ['velocity_5fd',['velocity_D',['../structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a8c799117e8ffeb965c274576a167b547',1,'mauve::types::sensor::GnssPVT']]],
  ['velocity_5fe',['velocity_E',['../structmauve_1_1types_1_1sensor_1_1GnssPVT.html#ac04f00ab31603ab69aab4eff00a93230',1,'mauve::types::sensor::GnssPVT']]],
  ['velocity_5fn',['velocity_N',['../structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a432aa41522a0cbdd02a4b0ec93dd4783',1,'mauve::types::sensor::GnssPVT']]],
  ['velocity_5fned',['velocity_NED',['../structmauve_1_1types_1_1sensor_1_1GnssVelocity.html#a5ff237b6943dcef2947270fe418e3cbf',1,'mauve::types::sensor::GnssVelocity']]],
  ['vertical_5facc',['vertical_acc',['../structmauve_1_1types_1_1sensor_1_1GnssPosition.html#a1138afefdc5418010161d4116f8baf72',1,'mauve::types::sensor::GnssPosition::vertical_acc()'],['../structmauve_1_1types_1_1sensor_1_1GnssPVT.html#a5339ad706b8c67763058a65f4de14680',1,'mauve::types::sensor::GnssPVT::vertical_acc()']]]
];
