var structmauve_1_1types_1_1sensor_1_1GnssVelocity =
[
    [ "heading_acc", "structmauve_1_1types_1_1sensor_1_1GnssVelocity.html#aa22d5caacf78ce1b7e8cf989e006e687", null ],
    [ "motion_heading", "structmauve_1_1types_1_1sensor_1_1GnssVelocity.html#a1e69d84c26f9ca42ef1cba96153d94f5", null ],
    [ "speed_acc", "structmauve_1_1types_1_1sensor_1_1GnssVelocity.html#ae834463ab9c70c7d43bf6bb5e669654b", null ],
    [ "vehicle_heading", "structmauve_1_1types_1_1sensor_1_1GnssVelocity.html#acddb91a5defe0cf61c6bc5cb4455505e", null ],
    [ "velocity_NED", "structmauve_1_1types_1_1sensor_1_1GnssVelocity.html#a5ff237b6943dcef2947270fe418e3cbf", null ]
];