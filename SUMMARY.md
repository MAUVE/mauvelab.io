# Summary

* [The MAUVE Toolchain](README.md)
* [Instructions](instructions/install.md)
  * [Install instructions](instructions/install.md)
  * [Real-Time system configuration](instructions/rt-config.md)
* [References](references.md)

## Reference Manual

* [MAUVE Runtime](manual/runtime/runtime.md)
  * [Configurable](manual/runtime/configurable.md)
  * [Shell](manual/runtime/shell.md)
    * [Property](manual/runtime/shell.md#shell-property)
    * [Port](manual/runtime/shell.md#shell-port)
  * [Core](manual/runtime/core.md)
  * [Finite State Machine](manual/runtime/fsm.md)
    * [Execution State](manual/runtime/fsm.md#fsm-execution)
    * [Synchronization State](/manual/runtime/fsm.md#fsm-synchronization)
    * [Constructing a simple FSM](/manual/runtime/fsm.md#fsm-constructing-simple)
    * [Constructing a complex FSM](/manual/runtime/fsm.md#fsm-constructing-complex)
  * [Interface](manual/todo.md)
  * [Architecture](manual/runtime/architecture.md)
    * [Components](manual/runtime/architecture.md#components)
    * [Resources](manual/runtime/architecture.md#resources)
    * [Configuration](manual/runtime/architecture.md#configuration)
  * [Deployment](manual/runtime/deployment.md)
  * [Logger](manual/runtime/logger.md)
* [MAUVE Tracing](manual/tracing/mauve_tracing.md)
  * [LTTng tracepoints](manual/tracing/mauve_tracing.md#lttng-tracepoints)
  * [Adding user tracepoints](manual/tracing/tracing.md)
  * [Activating LTTng traces](manual/tracing/mauve_tracing.md#activating-trace-collection)
  * [Advanced trace activation](manual/tracing/advanced.md)
* [MAUVE ROS Binding](manual/ros/mauve_ros.md)
  * [Publishing to a ROS topic](manual/ros/publisher.md)
  * [Subscribing to a ROS topic](manual/ros/subscriber.md)
  * [Publisher/Subscriber components](manual/ros/components.md)
  * [Filling ROS headers](manual/ros/header.md)
  * [Conversion functions](manual/ros/conversions.md)
    * [standard types](manual/ros/conversions.md#standard-types-conversions)
    * [sensor types](manual/ros/conversions.md#sensor-types-conversions)
    * [geometry types](manual/ros/conversions.md#geometry-types-conversions)
* [MAUVE Types](/manual/types.md)

## Tutorials

* Runtime Tutorials
  * [Hello World](tutorials/hello_world.md)
  * [Using Mauve properties](tutorials/property.md)
  * [Writer/Reader](tutorials/writer_reader.md)
  * [Writer/Reader with DataStatus](tutorials/writer_reader_status.md)
  * [Advanced Finite Sate Machine](tutorials/advanced_fsm.md)

## Traca

* [Introduction](traca/traca.md)
* [Mauve Trace](traca/trace.md)
* [Mauve Finite State Machine](traca/fsm.md)
* [TPTL](traca/tptl.md)

---

* [API Documentation](https://mauve.gitlab.io/api/html/)
* [Contact Us](contact.md)
* [License](license.md)
* [Copyright](COPYRIGHT.md)
