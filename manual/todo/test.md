This FSM is what we call a PeriodicStateMachine \(PSM\). A PSM is a subclass of FIniteStateMachine, and can be directly herited:

```c++
struct MyFSM : PeriodicStateMachine{};
```

You still need to redefine the PSM period and its _update\(\)_ method:

```c++
bool MyFSM::configure_hook() {
  period = ms_to_ns(100);
  return PeriodicStateMachine::configure_hook();
}

void MyFSM::update() {
  core().execMethod();
}
```
