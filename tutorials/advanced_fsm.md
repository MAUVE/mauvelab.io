**Tutorial available soon !**

The corresonding files can be found in the [mauve_tutorials](https://gitlab.com/MAUVE/mauve_tutorials) respository:
* [Complex Finite State Machine](https://gitlab.com/MAUVE/mauve_tutorials/tree/master/complex_fsm/)

...

<p align="center">
  <img src="complex_fsm.png">
</p>
