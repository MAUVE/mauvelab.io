
** Tutorial available soon ! **

The corresonding files can be found in the [mauve_tutorials](https://gitlab.com/MAUVE/mauve_tutorials) respository:
* [Shell Properties](https://gitlab.com/MAUVE/mauve_tutorials/tree/master/hello_property/src/hello_shell_property.cpp)
* [Core Properties](https://gitlab.com/MAUVE/mauve_tutorials/tree/master/hello_property/src/hello_core_property.cpp)
* [Finite State Machine Properties](https://gitlab.com/MAUVE/mauve_tutorials/tree/master/hello_property/src/hello_fsm_property.cpp)
