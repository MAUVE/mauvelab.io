## Core

The Core describes both the algorithmic part and the inner data of the component/resource. Thus, the Core defines attributes and methods. The Core depends on a Shell which means its methods can interact with the Shell.

A Core is created by extending the class `mauve::runtime::Core`.
The `Core` is a template class, templated by the `Shell`.

```c++
#include "mauve/runtime.hpp"

struct MyCore: public mauve::runtime::Core<MyShell> {};
```

A Core is [configurable](configurable.md), consequently you can override its configure and cleanup hooks.

A Core can have properties, just as Shells (see [Shell properties](shell.md#shell-property])).

Core methods can use Shell properties and ports, like depicted in this code snapshot:

```c++
void MyCore::my_method()
{
  int received_age = shell().read_age.read();
  if (received_age < shell().age)
    shell().write_age(received_age + 1);
}
```

This method reads the data received in port *read_age*. It then compares the received
data to the shell property _age_. If the received data is smaller, the value
plus one is written to port *write_age*. 
