var structmauve_1_1runtime_1_1SharedDataInterface =
[
    [ "C", "structmauve_1_1runtime_1_1SharedDataInterface.html#a0b7b8feb7cb545a47065ff4f4b563362", null ],
    [ "read", "structmauve_1_1runtime_1_1SharedDataInterface.html#a3b71d2c1dae6a0bd450ec54ec00ca201", null ],
    [ "read_status", "structmauve_1_1runtime_1_1SharedDataInterface.html#a2de875dfb7ebd3387f4d70084d5ddbb6", null ],
    [ "read_value", "structmauve_1_1runtime_1_1SharedDataInterface.html#a044245ff741003c5af4c81c17cefe43e", null ],
    [ "reset", "structmauve_1_1runtime_1_1SharedDataInterface.html#a47d7793172df5a68e8563d00ac9b5ec6", null ],
    [ "write", "structmauve_1_1runtime_1_1SharedDataInterface.html#a13440de59873ca2fb5827e88444fa602", null ]
];