# MAUVE Tracing library

The MAUVE tracepoints defined in the MAUVE runtime are designed to collect
common events of the MAUVE execution (service calls, component executions).

The MAUVE tracing library also provides some tracepoints to add
user-defined messages, specific to the application.

## User tracepoints

The available user tracepoints are:
* _event_, to trace a specific event;
* _start_, to trace the start of some processing;
* _end_, to trace the end of some processing.

These tracepoints have a unique field: a string message.

## Adding user tracepoints to you code

The following code snippet shows how to use MAUVE tracing into some user
code. In this example, MAUVE tracing is used inside a ROS node: MAUVE user
tracepoints are not dependent on the MAUVE runtime and can be used everywhere.

```c++

#include <ros/ros.h>
#include <mauve/tracing.hpp>

int main(int argc, char** argv) {

  ros::init(argc, argv, "my_node_with_traces");
  ros::NodeHandle n("~");
  ros::Rate loop_rate(1.0);

  mauve::tracing::trace_event("starting ROS node");

  while (ros::ok()) {
    mauve::tracing::trace_start("loop");
    /* do some stuff */
    ros::spinOnce();
    mauve::tracing::trace_end("loop");
    loop_rate.sleep();
  }

  return 0;
}
```

## Resulting traces

To collect traces while your program is running, see
[Activating traces](mauve_tracing.md#Activating-trace-collection).

Then, using _babeltrace_, your traces will contain your user tracepoints:

```sh
$ babeltrace ~/lttng-traces/ros-20170712-160932/
(...)
[16:09:55.575780087] (+0.000036548) wdcsd002h mauve:event: { cpu_id = 1 }, { message = "starting ROS node" }
(...)
[16:10:07.695759558] (+0.019905315) wdcsd002h mauve:end: { cpu_id = 1 }, { message = "loop" }
[16:10:07.775856051] (+0.080096493) wdcsd002h mauve:start: { cpu_id = 1 }, { message = "loop" }
[16:10:07.795819324] (+0.019963273) wdcsd002h mauve:end: { cpu_id = 1 }, { message = "loop" }
[16:10:07.875856567] (+0.080037243) wdcsd002h mauve:start: { cpu_id = 1 }, { message = "loop" }
[16:10:07.895743429] (+0.019886862) wdcsd002h mauve:end: { cpu_id = 1 }, { message = "loop" }
```
