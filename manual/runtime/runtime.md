# MAUVE Runtime

The MAUVE Runtime is a component-based middleware
that allows to perform software architecture reconfigurations with a
focus on real-time constraints.

The software architecture is described as components connected to each others through resources and ports, and configured through properties. The behavior of each component is defined by a finite state machine whose activity is managed by a dedicated real-time task. Components are instantiated and their data ports connected together to form the system architecture which is initialized and launched by a specific tool: the deployer.

A component is made of:
* a [Shell](manual/shell.md), defining the interface of the component;
* a [Core](manual/core.md), defining the inner data and functions of the component;
* a [FSM](manual/FSM.md), defining the temporal behavior of the component.

A resource is made of:
* a [Shell](manual/shell.md), defining the interface of the resource;
* a [Core](manual/core.md), defining the inner data and functions of the resource;
* an [Interface](manual/Interface.md), defining the services provided by the resource.

An [Architecture](manual/architecture.md) is then built by instantiating, connecting and configuring components and resources.

Every element in MAUVE Runtime is [Configurable](manual/configuragle.md), meaning that the element has an internal state that limit when is modifiable within the element.
