# Hello World

The main purpose of this tutorial is to introduce the basic concepts of **Mauve** components, architecture and delployment.

In this example we will create a mauve architecture containing only one component. This component periodicaly display the famous "Hello World" message.

The corresponding [files](https://gitlab.com/MAUVE/mauve_tutorials/tree/master/hello_world) can be found in the [mauve_tutorials](https://gitlab.com/MAUVE/mauve_tutorials) respository.

## Using mauve runtime

In order to use mauve runtime we must first include the library.

```c++
#include "mauve/runtime.hpp"
using namespace mauve::runtime;
```

## Shell

A component is made of: a shell, a core and a finite state machine. The shell objective is to contains ports which are used to communicate with other components.
In this simple example, there is only one component. Thus, the shell is obviously empty.

```c++
struct HelloShell: public Shell {};
```

## Core

A component core contains the algorithmic part of the component. In this example, the core only has an `update` method to display our famous message.

```c++
struct HelloCore: public Core<HelloShell> {
  void update() {
    std::cout << "Hello World !" << std::endl;
  }
};
```

## Finite State Machine

A component FSM aims to schedule the algorithmic part of the component (its core). A Mauve finite state machine is made of two kind of states:
* execution states: execute core methods.
* synchronization states: wait until the internal clock of the component reaches a specific value.

In this example, we create a periodic execution. Consequently, the finite state machine is made of two states:
* an execution state named *Update* state, which executes the method `update()` of the previously defined core;
* a synchronization state with a clock value equals to *1 sec*.

The following figure represent this finite state machine:
<p align="center">
  <img src="hello_world_fsm.png">
</p>

```c++
struct HelloFSM: public FiniteStateMachine<HelloShell, HelloCore> {
  // Create the Execution state: run the update methode of the core
  ExecState<HelloCore>    & U = mk_execution("Update", &HelloCore::update);
  // Create the synchronization state: 1 sec
  SynchroState<HelloCore> & W = mk_synchronization("Wait", sec_to_ns(1));

  bool configure_hook() override {
    set_initial(U); // set the initial state to Update
    set_next(U, W); // Wait after Update
    set_next(W, U); // Update after Wait
    return true;
  }
};
```

## Architecture

The architecture contains only one component named "hello_cpt". This component is defined by the previously defined *HelloShell*, *HelloCore* and *HelloFSM*.

```c++
struct HelloArchi: public Architecture {
  // Create the component based on the previously defined Shell, Core, FSM
  Component<HelloShell, HelloCore, HelloFSM> & hello_cpt = mk_component<HelloShell, HelloCore, HelloFSM>("hello_cpt");
};
```


## Deployment

The deployment consists in different steps:
* create the architecture;
* create a deployer for this architecture;
* configure the architecture;
* create the tasks of the components of the architecture;
* activate those tasks;
* start the deployer and the tasks;
* then, the deployer can run until a signal is received (`Ctrl-C`);
* finaly the deployer stop the tasks.

```c++
int main(int argc, char const *argv[]) {
  // Create the architecture
  HelloArchi architecture;

  // Create a deployer for the architecture
  AbstractDeployer* deployer = mk_abstract_deployer(&architecture);

  // Configure the architecture
  architecture.configure();

  // Create the component task
  deployer->create_tasks();
  // Activate the component task
  deployer->activate();

  // Start the deployer and the component task
  deployer->start();

  // Deployer is running until C-C
  deployer->loop();

  // Stop the deployer and the component task
  deployer->stop();

  return 0;
}
```


## Cmake

In order to compile this example we ask catkin to find the *mauve runtime* library, and we add the dependency.

```cmake
cmake_minimum_required(VERSION 2.8.3)
project(hello_world)

## C++ 11
set ( CMAKE_CXX_STANDARD 11 )
set ( CMAKE_CXX_EXTENSIONS False )

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS mauve_runtime)

catkin_package(
  INCLUDE_DIRS include
  CATKIN_DEPENDS mauve_runtime
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

add_executable ( hello_world src/hello_world.cpp )
target_link_libraries ( hello_world mauve_runtime )
```

## Real-time execution

At the program is executed in real-time displaying the "Hello World !" periodicaly, every *1* second.

```
$ mauve_ws/devel/lib/hello_world/hello_world
Hello World !
Hello World !
Hello World !
...
```
