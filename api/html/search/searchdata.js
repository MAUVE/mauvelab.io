var indexSectionsWithContent =
{
  0: "_abcdefghijlmnopqrstuvwxyz~",
  1: "acdefghijlmopqrstuvw",
  2: "m",
  3: "acdefgilmnoprstuw~",
  4: "_abcdefghilmnoprstvwxyz",
  5: "acr",
  6: "cfgp",
  7: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "related"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Friends"
};

