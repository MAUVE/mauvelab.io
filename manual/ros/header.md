# MAUVE ROS

## ROS Headers

The conversions functions provided by MAUVE ROS do not manage the header field of ROS messages.

The following code snippets show how to define your conversion function with header.
They consider a publisher or subscriber resource that convert a ROS `geometry_msgs::PoseStamped` to/from a
MAUVE `mauve::types::geometry::Pose2D`.

The following publisher fills the frame id of the ROS message header:

```c++
pose_publisher.shell().conversion.set_value([](const mauve::types::geometry::Pose2D& data, geometry_msgs::PoseStamped& msg) {
  msg.header.frame_id = "base_link";
  return mauve::ros::conversions::convert(data, msg.pose);
});
```

The following subscriber only accept ROS message with header 'base_link':

```c++
pose_subscriber.shell().conversion.set_value([](const geometry_msgs::PoseStamped& msg, mauve::types::geometry::Pose2D& data) {
  if (msg.header.frame_id == "base_link")
    return mauve::ros::conversions::convert(msg.pose, data);
  else return false;
});
```
