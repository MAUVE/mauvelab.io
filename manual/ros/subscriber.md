# MAUVE ROS

## Subscribe data from ROS

The basic element to subscribe some data from ROS is the MAUVE _SubscriberResource_.

The _SubscriberResource_ has a [Shell](/manual/runtime/shell.md) with:

- a _topic_ property to define the ROS topic to subscribe to
- a _conversion_ property to define the conversion function between your internal type and a ROS message

The _SubscriberResource_ has an [Interface](/manual/runtime/interface.md) with:

- a _read_ service so that a component can _read_ the received data along with a status
- a *read_value* service so that a component can _read_ the received data (independently of the status)
- a *read_status* service so that a component can _read_ the received data status

The _SubscriberResource_ is templated by the internal MAUVE type and the ROS message type.

## Code samples

Here is a simple example of an architecture with a component that subscribes a string from ROS through a _SubscriberResource_.

First, include the _mauve_ros_ headers:

```c++
#include <mauve/ros.hpp>
```

In your architecture, add a _SubscriberResource_ that convert a ROS _String_ into a _string_:

```c++
mauve::ros::SubscriberResource<std_msgs::String,std::string> & string_subscriber =
  mk_resource<mauve::ros::SubscriberResource<std_msgs::String,std::string>>("string_subscriber");
```

In the architecture _configure_hook_, connect the _ReadPort_ of the component to the resource, and configure the subscriber properties:

```c++
bool configure_hook() {
  my_component.shell().read_port.connect(string_subscriber.interface().read);
  string_subscriber.shell().topic = "my_str";
  string_subscriber.shell().conversion.set_value(mauve::ros::conversions::convert<std_msgs::String,std::string>);
}
```

The conversion function used by the subscriber is one of the converters provided by MAUVE. The complete list of MAUVE/ROS conversion functions is lister [there](conversions.md).

Instead of using the provided conversions, you can specify your own conversion by passing a function pointer to the subscriber property, of by defining a lambda function. For instance, the following code defines a custom function:

```c++
string_subscriber.shell().conversion.set_value([](const std_msgs::String& msg, std::string& data) -> bool {
  if (msg.data.empty()) return false;
  else {
    data = msg.data;
    return true;
  }
});
```

This conversion function will not accept empty strings.
