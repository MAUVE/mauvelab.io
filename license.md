## License

All the MAUVE Toolchain packages are licensed under the
[GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0),
that permits use of the libraries in proprietary programs.

![LGPLv3](./images/lgplv3.png)
