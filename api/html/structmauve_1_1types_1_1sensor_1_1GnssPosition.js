var structmauve_1_1types_1_1sensor_1_1GnssPosition =
[
    [ "altitude", "structmauve_1_1types_1_1sensor_1_1GnssPosition.html#aada89cdc901ff39c9c25c1fa06615d7c", null ],
    [ "fix_type", "structmauve_1_1types_1_1sensor_1_1GnssPosition.html#aa2c1b214c35d25ae771d5e780efe497b", null ],
    [ "horizontal_acc", "structmauve_1_1types_1_1sensor_1_1GnssPosition.html#a4b8fc0d4da2d24316320a0f9cbfbf52e", null ],
    [ "latitude", "structmauve_1_1types_1_1sensor_1_1GnssPosition.html#af93d4062f0734cb2855ad6d2b2819090", null ],
    [ "longitude", "structmauve_1_1types_1_1sensor_1_1GnssPosition.html#aed14243a49a9767b44afe67d4d280df9", null ],
    [ "time_acc", "structmauve_1_1types_1_1sensor_1_1GnssPosition.html#a7cca14339947b544e01fc5657d078edf", null ],
    [ "timestamp", "structmauve_1_1types_1_1sensor_1_1GnssPosition.html#a729ae3f54332aa73b58c2514f8b113e3", null ],
    [ "vertical_acc", "structmauve_1_1types_1_1sensor_1_1GnssPosition.html#a1138afefdc5418010161d4116f8baf72", null ]
];