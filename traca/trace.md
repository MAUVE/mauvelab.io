## Trace analysis

Once traca launched a *trace* object is created.

```sh
$ java -cp release/mauve_traca.jar mauve.traca.pointwise.MainConsole

         \,,,/
         (o o)
-----oOOo-(_)-oOOo-----
-------------------- loading --------------------
trace: mauve.traca.pointwise.MauveTrace = MauveTrace()
--------------------  ready  --------------------
```

A Mauve trace contains a sequence of *Mauve Events*. At start the trace is empty.
```scala
Mauve [1]: trace
res1: mauve.traca.pointwise.MauveTrace = MauveTrace()
```


## Loading a CTF/Mauve trace

In order to load a lttng trace with Mauve events we have to use the comment ":open":

```scala
Mauve [2]: :open lttng-traces/summit-ublox-20180327-152620/ust/uid/1000/64-bit
Mauve [3]: trace.size
res2: Int = 81411
```

Notice that the ":open" command have the file path completion.

## Trace components

```scala
Mauve [6]: trace.components
res3: scala.collection.immutable.Set[String] = Set(ublox_driver)
```

```scala
Mauve [7]: trace.componentCpu("ublox_driver")
res4: scala.collection.immutable.Set[Int] = Set(0)
```
