var NAVTREE =
[
  [ "The MAUVE Toolchain", "index.html", [
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"classmauve_1_1runtime_1_1Component.html#aaebf9016ffeb5c72af76887adc9d8270",
"classmauve_1_1runtime_1_1Resource.html#a1682bdac9e3cdc2ce52c9263be0ad8fd",
"structmauve_1_1base_1_1MuxPriorityReadPortShell.html#a2ad61e2c0c86af550edf2cda9f3e03c8",
"structmauve_1_1types_1_1sensor_1_1GnssPVT.html#af08c5653289f078f03327e04e1c161e6"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';