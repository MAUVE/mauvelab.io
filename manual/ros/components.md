# MAUVE ROS

## Publisher and Subscriber components

MAUVE ROS provides a component version of the publisher and subscriber, to be
used for the cases where:
* the data to publish/subscribe is not written/read from a component
* you need to manage the rate at which data are published/subscribed to ROS

### PublisherComponent

The _PublisherComponent_ has the same properties than the
[_PublisherResource_](publisher.md) (i.e. _topic_ and _conversion_).

It also has a _ReadPort_ on which the component reads data and publish it to ROS.

The component FSM has a _period_ property to manage the publishing rate.

The following code snippet shows how to use the _PublisherComponent_:

```c++
struct MyArchitecture : public mauve::runtime::Architecture {
  (...)
  mauve::ros::PublisherComponent<std::string, std_msgs::String> & string_publisher =
    mk_component<mauve::ros::PublisherComponent<std::string, std_msgs::String>>("string_publisher");
  mauve::runtime::SharedData<std::string> & text =
    mk_resource<mauve::runtime::SharedData<std::string>>("text", "");
  (...)
  bool configure_hook() {
    (...)
    string_publisher.shell().read_port.connect(text.interface().read);
    (...)
    string_publisher.shell().topic = "my_text";
    string_publisher.shell().conversion.set_value(mauve::ros::conversions::convert<std::string,std_msgs::String>);
    string_publisher.fsm().period = mauve::runtime::ms_to_ns(100);
    (...)
    return mauve::runtime::Architecture::configure_hook();
  }
};
```

### SubscriberComponent

The _SubscriberComponent_ behaves similarly, with a _WritePort_ to write data received trom ROS.

The following code snippet shows how to use the _SubscriberComponent_:

```c++
struct MyArchitecture : public mauve::runtime::Architecture {
  (...)
  mauve::ros::SubscriberComponent<std::string, std_msgs::String> & string_subscriber =
    mk_component<mauve::ros::SubscriberComponent<std::string, std_msgs::String>>("string_subscriber");
  mauve::runtime::SharedData<std::string> & text =
    mk_resource<mauve::runtime::SharedData<std::string>>("text", "");
  (...)
  bool configure_hook() {
    (...)
    string_subscriber.shell().write_port.connect(text.interface().write);
    (...)
    string_subscriber.shell().topic = "my_text";
    string_subscriber.shell().conversion.set_value(mauve::ros::conversions::convert<std_msgs::String,std::string>);
    string_subscriber.fsm().period = mauve::runtime::ms_to_ns(100);
    (...)
    return mauve::runtime::Architecture::configure_hook();
  }
};
```
