# Deployment

A deployment is the process of initialising an architecture, creating threads corresponding to components, and synchronizing and strating these threads.

A *Deployment* is then the main entry point of your MAUVE application. You can create a deployment either as a plain C++ executable, or as a library that will be loaded using the Scala Deployer.

## Deployment Processes

The following code defines a deployment process that can be built as an executable:

```c++
int main(int argc, char** argv) {
  int r = ::mauve::runtime::main(argc, argv);
  if (r > 0) return r;

  auto archi = new MyArchitecture();
  auto depl = mk_abstract_deployer(archi);
  archi->configure();
  depl->create_tasks();
  depl->activate();
  depl->start();
  depl->loop();
  depl->stop();
  archi->cleanup();
  return 0;
}
```

The first line calls an helper function, `mauve::runtime::main`, that provides
options to load configuration files ([logging](logger.md) and [property](property_configuration.md) configurations).
Then an architecture objet *archi* is created, and bind to a deployer *depl* using `mk_abstract_deployer`.
The deployement steps are then to:

* configure the architecture (`archi->configure()`)
* create component tasks (`depl->create_tasks()`)
* activate component tasks (`depl->activate()`)
* start the component tasks (`depl->start()`)
* suspend the deployer execution while the architecture components execute (`depl->loop()`)

If the process is terminated (using a signal for instance), then steps to cleanly end the deployment are:

* stopping the component tasks (`depl->stop()`)
* cleaning the architecture (`archi->cleanup()`)

## Scala Deployment

**Documentation In Progress**
