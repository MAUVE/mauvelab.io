var structmauve_1_1types_1_1sensor_1_1GnssTime =
[
    [ "day", "structmauve_1_1types_1_1sensor_1_1GnssTime.html#a8aff7f1b6684c409f1457853f2f02515", null ],
    [ "hour", "structmauve_1_1types_1_1sensor_1_1GnssTime.html#a6ff2ce2c4660b76a6cc906df0ee97e66", null ],
    [ "itow", "structmauve_1_1types_1_1sensor_1_1GnssTime.html#a01a59350d4a19095346a7d02fed62f49", null ],
    [ "min", "structmauve_1_1types_1_1sensor_1_1GnssTime.html#a33838b38e8624485ef1f80234608784a", null ],
    [ "month", "structmauve_1_1types_1_1sensor_1_1GnssTime.html#ab2e17566e5756b546babbb5e0955845b", null ],
    [ "nano", "structmauve_1_1types_1_1sensor_1_1GnssTime.html#a7a6b23af79513916c78b0992a857079b", null ],
    [ "sec", "structmauve_1_1types_1_1sensor_1_1GnssTime.html#af3336f765ba42227fd20e1617443f9c4", null ],
    [ "t_acc", "structmauve_1_1types_1_1sensor_1_1GnssTime.html#a958937e9bfbde101f801ad5c83632961", null ],
    [ "valid", "structmauve_1_1types_1_1sensor_1_1GnssTime.html#a04484f96bd2d3258583fbf97404633af", null ],
    [ "year", "structmauve_1_1types_1_1sensor_1_1GnssTime.html#a2420f4b0883ed2e5247f793aa4ea25dc", null ]
];