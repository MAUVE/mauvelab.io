# MAUVE Types

MAUVE Types are splitted into _geometry_ types, relative to geometric data (similarly to ROS geometry\_msgs), and _sensor_ types, relative to sensor data
(like ROS sensor\_msgs).

To use a type, you just need to include the corresponding header, and to mave your package depend on `mauve_types`. For instance:

```
#include <mauve/types/geometry/Point2D.hpp>
```

If you want to use several types, you can directly include a helper header:
```
#include <mauve/types.hpp>
```

The list of MAUVE types is directly available in the MAUVE API documentation:

* [geometry types](/api/html/namespacemauve_1_1types_1_1geometry.html)
* [sensor types](/api/html/namespacemauve_1_1types_1_1sensor.html)
